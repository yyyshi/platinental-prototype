<?php

$this->get('/', 'PageController@page')->name('home');

$this->get('{page}', 'PageController@page')->name('page')->where('page', '[a-z0-9\/\-\_]+');
