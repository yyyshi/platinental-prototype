<?php

namespace App\Http\ViewComposers;

use Illuminate\Support\Collection;
use Illuminate\View\View;

class MenuComposer
{

    public function compose(View $view)
    {
        $menu = new Collection([
                                   (object)[
                                       'label' => 'About',
                                       'items' => [
                                           (object)[
                                               'label' => 'The Platinental',
                                               'url'   => route('page', 'the'),
                                           ],
                                           (object)[
                                               'label' => 'Press',
                                               'url'   => route('page', 'press'),
                                           ],
                                           (object)[
                                               'label' => 'FAQ/Help',
                                               'url'   => route('page', 'faq'),
                                           ],
                                       ],
                                   ],
                                   (object)[
                                       'label' => 'Medical Tourism',
                                       'items' => [
                                           (object)[
                                               'label' => 'Roundblock Facelift',
                                               'url'   => route('home'),
                                           ],
                                           (object)[
                                               'label' => 'Facelift',
                                               'url'   => route('home'),
                                           ],
                                           (object)[
                                               'label' => 'Chin Liposuction',
                                               'url'   => route('home'),
                                           ],
                                           (object)[
                                               'label' => 'Roundblock Facelift',
                                               'url'   => route('home'),
                                           ],
                                           (object)[
                                               'label' => 'Facelift',
                                               'url'   => route('home'),
                                           ],
                                           (object)[
                                               'label' => 'Chin Liposuction',
                                               'url'   => route('home'),
                                           ],
                                           (object)[
                                               'label' => 'Roundblock Facelift',
                                               'url'   => route('home'),
                                           ],
                                           (object)[
                                               'label' => 'Facelift',
                                               'url'   => route('home'),
                                           ],
                                           (object)[
                                               'label' => 'Chin Liposuction',
                                               'url'   => route('home'),
                                           ],
                                           (object)[
                                               'label' => 'Facelift',
                                               'url'   => route('home'),
                                           ],
                                           (object)[
                                               'label' => 'Chin Liposuction',
                                               'url'   => route('home'),
                                           ],
                                           (object)[
                                               'label' => 'Roundblock Facelift',
                                               'url'   => route('home'),
                                           ],
                                           (object)[
                                               'label' => 'Facelift',
                                               'url'   => route('home'),
                                           ],
                                           (object)[
                                               'label' => 'Chin Liposuction',
                                               'url'   => route('home'),
                                           ],
                                       ],
                                   ],
                                   (object)[
                                       'label' => 'Online Services',
                                       'items' => [
                                           (object)[
                                               'label' => 'Roundblock Facelift',
                                               'url'   => route('home'),
                                           ],
                                           (object)[
                                               'label' => 'Facelift',
                                               'url'   => route('home'),
                                           ],
                                           (object)[
                                               'label' => 'Chin Liposuction',
                                               'url'   => route('home'),
                                           ],
                                       ],
                                   ],
                                   (object)[
                                       'label' => 'Store',
                                       'url'   => route('page', 'store'),
                                   ],
                                   (object)[
                                       'label' => 'Partners',
                                       'url'   => route('page', 'partners'),
                                   ],
                                   (object)[
                                       'label' => 'Contact',
                                       'url'   => route('page', 'contact'),
                                   ],
                               ]);

        $userMenu = new Collection([
                                       (object)[
                                           'label' => 'Login',
                                           'url'   => route('page', 'login'),
                                       ],
                                       (object)[
                                           'label' => 'Cart',
                                           'url'   => route('page', 'cart'),
                                       ],
                                   ]);

        if( !request()->is('/')) {
            $menu = $this->markActiveMenu($menu, url()->current());
        }

        $view->with('MENU', $menu);
        $view->with('USER_MENU', $userMenu);
    }

    /**
     * Отмечает активный элемент в $menu
     * @param  Collection $menu [description]
     * @param  String     $url  [description]
     * @return Collection
     */
    public function markActiveMenu($menu, $url):Collection
    {
        $url = trim($url, '/');
        $urlLen = strlen($url);
        return $menu->map(function($item) use ($url, $urlLen) {
            $itemUrl = trim($item->url, '/');
            if(strpos($url, $itemUrl) === 0) {
                $item->active = true;
                if(strlen($itemUrl) < $urlLen) {
                    $item->hasActiveChild = true;
                }
            }
            return $item;
        });
    }

}
