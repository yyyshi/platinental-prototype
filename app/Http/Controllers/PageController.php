<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{

    public function page(Request $request)
    {
        return view('layouts.page');
    }

}
