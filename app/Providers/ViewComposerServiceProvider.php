<?php

namespace App\Providers;

use App\Http\ViewComposers\MenuComposer;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use View;

class ViewComposerServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     * @return void
     */
    public function boot()
    {
        // Global Injects
        // View::share('dateInterval', app('date-interval-string-format'));
        // View::share('moneyFormat', app('money-string-format'));
        // View::share('dateFormat', app('date-string-format'));
        // View::share('stringUtils', app('string-utils'));
        // View::share('markdownParser', app('markdown-parser'));

        // Globals
        View::share('IS_ROOT', request()->is('/'));
        View::share('AUTH', $this->guard());

        View::composer('layouts.page', MenuComposer::class);
        // View::composer('pages.partials.footer.footer', MainMenuComposer::class);
    }

    /**
     * Register the application services.
     * @return void
     */
    public function register()
    {
        //
    }

    protected function guard():Guard
    {
        return Auth::guard('web');
    }

    protected function getActiveSubscription(Guard $guard)
    {
        $userId = 0;
        if($guard->check()) {
            $userId = $guard->user()->getId();
        }

        return $this->app->make(GetActiveSubscription::class)
            ->getOfUser(UserId::fromRaw($userId));
    }

}
