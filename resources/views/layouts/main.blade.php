<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>@yield('title', 'The Platinental')</title>
    <meta name="viewport" content="width=device-width, user-scalable=1">
    <meta name="description" content="@yield('description', '')">
    <meta property="og:title" content="@yield('title', 'The Platinental')">
    <meta property="og:description" content="@yield('description', '')">
    <meta property="og:image" content="@yield('image', url(''))">
    <meta property="og:url" content="{{ url()->current() }}">
    <meta property="og:site_name" content="The Platinental">

    <link href="/styles/basic.css?{{ time() }}" rel="stylesheet">
    <link href="/styles/layout.css?{{ time() }}" rel="stylesheet">
    <link href="/styles/header.css?{{ time() }}" rel="stylesheet">
    <link href="/styles/menu.css?{{ time() }}" rel="stylesheet">
    @stack('styles')

</head>

<body class="no-js @yield('layoutClass')" data-module="page-module">

    @yield('content')

    {{-- Scripts --}}
    {{--@include('layouts.scripts.vendor-common')
    @include('layouts.scripts.vendor-site')
    @include('layouts.scripts.common')
    @include('layouts.scripts.site')
    @stack('scripts')
    @include('layouts.scripts.app')--}}

</body>
</html>
