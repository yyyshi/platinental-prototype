@if(isset($item->items))
    <input type="radio" name="main-menu" id="menu-{{ str_slug($item->label) }}"
           class="main-menu__visibler">
    <li class="main-menu__item main-menu__item_has_subitems">
        <label for="menu-{{ str_slug($item->label) }}" class="main-menu__label">
            {{ $item->label }}
        </label>
        <div class="main-menu__subitems-wrapper">
            <ul class="main-menu__subitems">
                @foreach($item->items as $subitem)
                    <li class="main-menu__subitem">
                        <a href="{{ $subitem->url }}" class="main-menu__link">
                            {{ $subitem->label }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </li>
@else
    <li class="main-menu__item main-menu__label {{ isset($item->desktopHide) && $item->desktopHide ? 'main-menu__item_desktop_hide' : '' }}">
        <a href="{{ $item->url }}" class="main-menu__link">
            {{ $item->label }}
        </a>
    </li>
@endif
