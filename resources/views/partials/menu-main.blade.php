@if($MENU)
    <div class="main-menu__wrapper">
        <form class="main-menu" id="main-menu">
            <ul class="main-menu__items">
                @foreach($MENU as $item)
                    @if($loop->last)
                        @break
                    @endif
                    @include('partials.menu-main-item', compact('item'))
                    {{--<li class="main-menu__divider"></li>--}}
                @endforeach
            </ul>
            <ul class="main-menu__last-item">
                @include('partials.menu-main-item', ['item' => $MENU->last()])
            </ul>
            <ul class="main-menu__user-items">
                <li class="main-menu__divider"></li>
                @foreach($USER_MENU as $item)
                    @include('partials.menu-main-item', compact('item'))
                @endforeach
            </ul>
        </form>
    </div>

    <script>
        (function() {
            'use strict';

            var mainMenu = document.querySelector('.main-menu');

            if(isTouch()) {
                openedMenuClickInit(mainMenu);
            }
            else {
                hoverMenuInit(mainMenu);
            }

            dropdownHeightCorrectorInit(mainMenu);

            function isTouch() {
                return 'ontouchstart' in window || window.innerWidth <= 1024;
            }

            function hoverMenuInit(menu) {
                // Открываем пункт меню при наведении
                menu.addEventListener('mouseover', function(e) {
                    var el = e.target;
                    if(el.tagName.toLowerCase() !== 'label') {
                        // Если зашли на другой пункт меню, закроем открытый
                        if(el.className.indexOf('main-menu__label') !== -1) {
                            menu.reset();
                        }
                        return;
                    }
                    el.click();
                });
            }

            function dropdownHeightCorrectorInit(menu) {
                menu.addEventListener('change', function(e) {
                    var item = e.target.nextElementSibling;
                    var dropHeight = item.querySelector('.main-menu__subitems').clientHeight;
                    // Установим высоту выпадушки ее врапперу
                    item.querySelector('.main-menu__subitems-wrapper').style.height = dropHeight + 'px';
                });
            }

            // Обработка клика на уже открытом пункте меню
            function openedMenuClickInit(menu) {
                menu.addEventListener('click', function(e) {
                    var el = e.target;
                    if(el.tagName.toLowerCase() !== 'label') {
                        return;
                    }
                    var controller = document.getElementById(el.getAttribute('for'));
                    if(controller && controller.checked) {
                        e.preventDefault(); // Чтобы клик опять не отметил контрол
                        menu.reset();
                    }
                });
            }
        })();
    </script>
@endif
