<!--suppress CheckEmptyScriptTag -->

<input type="checkbox" id="main-menu-switcher">

<div class="header">

    <div class="header__item menu-control">
        <label for="main-menu-switcher" class="menu-conrol__controller menu-open">
            <svg viewBox="0 0 17 12" xmlns="http://www.w3.org/2000/svg">
                <g stroke="#252525" stroke-width="2" fill="none">
                    <path d="M1 11h15" />
                    <path d="M1 1h15" />
                    <path d="M1 6h15" />
                </g>
            </svg>
        </label>
        <label for="main-menu-switcher" class="menu-conrol__controller menu-close">
            <svg viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg">
                <g stroke="#252525" stroke-width="2" fill="none">
                    <path d="M2.274 2.274l9.449 9.449" />
                    <path d="M2.274 11.726l9.449-9.449" />
                </g>
            </svg>
        </label>
    </div>

    <div class="header__item">
        <div class="logo">
            <a href="/">
                <img src="/i/logo.png" alt="Platinental" class="logo__img">
            </a>
        </div>
    </div>

    <div class="header__item">
        <div class="user-menu">
            <div class="user-menu__item user-menu__item_login">
                <a href="" class="main-menu__link">
                    Login
                </a>
            </div>
            <div class="user-menu__item user-menu__item_cart user-menu__cart_empty">
                <a href="" class="cart-icon cart-empty-icon">
                    <svg width="20" height="21" viewBox="0 0 20 21" xmlns="http://www.w3.org/2000/svg">
                        <g stroke="#252525" stroke-width="2" fill="none">
                            <path d="M1 8h18v12h-18z" />
                            <path d="M15 6c0-2.761-2.239-5-5-5s-5 2.239-5 5" />
                        </g>
                    </svg>
                </a>
                <a href="" class="cart-icon cart-not-empty-icon">
                    <svg width="20" height="21" viewBox="0 0 20 21" xmlns="http://www.w3.org/2000/svg">
                        <g transform="translate(-1092 -49) translate(1092 50)" fill="none">
                            <path stroke="#252525" stroke-width="2" d="M1 7h18v12h-18z" />
                            <path d="M15 5c0-2.761-2.239-5-5-5s-5 2.239-5 5" stroke="#252525" stroke-width="2" />
                            <circle fill="#252525" cx="10" cy="13" r="2" />
                        </g>
                    </svg>
                </a>
            </div>
        </div>
        <div class="lang">
            <a href="">
                РУ
            </a>
        </div>
    </div>

</div>
